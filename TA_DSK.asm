   name "Be Brave, Be Humble"
   org 100h
    mov ax, 3
    int 10h
    mov ax, 1003h
    mov bx, 0
    int 10h
    mov ax, 0b800h
    mov ds, ax
    mov [02h], 'B'
    mov [04h], 'e'
    mov [06h], ' '
    mov [08h], 'B'
    mov [0ah], 'r'
    mov [0ch], 'a'
    mov [0eh], 'v'
    mov [10h], 'e'
    mov [12h], ','
    mov [14h], ' '
    mov [16h], 'B'
    mov [18h], 'e'
    mov [1ah], ' '
    mov [1ch], 'H'
    mov [1eh], 'u'
    mov [20h], 'm'
    mov [22h], 'b'
    mov [24h], 'l'
    mov [26h], 'e'
    mov cx, 19
    mov di, 03h
    c: mov [di], 01111011b
      add di, 2
      loop c
      mov ah, 0
      int 16h
    Ret 